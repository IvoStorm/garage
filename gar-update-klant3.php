<?php
$klant_id = $_POST ["klantidvak"];
$klantnaam = $_POST ["klantnaamvak"];
$klantadres = $_POST ["klantadresvak"];
$klantpostcodee = $_POST ["klantpostcodeevak"];
$klantplaats = $_POST ["klantplaatsvak"];

require_once "garage_connect.php";

$sql = $conn->prepare("update klant set klantnaam = :klantnaam, klantadres = :klantadres, klantpostcodee = :klantpostcodee, klantplaats = :klantplaats where klant_id = :klant_id");

$sql->execute(["klant_id" => $klant_id, "klantnaam" => $klantnaam, "klantadres" => $klantadres, "klantpostcodee" => $klantpostcodee, "klantplaats" => $klantplaats]);

echo "De klant is veranderd <br/>";
echo "<a href='garagemenu.html'> terug naar het menu </a>";

