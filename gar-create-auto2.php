<?php

$kenteken = $_POST ["kentekenvak"];
$automerk = $_POST ["amerkvak"];
$autotype = $_POST ["aautotypevak"];
$kilometerstand = $_POST ["akmstandvak"];
$klant_id = $_POST ["klantidvak"];

require_once "garage_connect.php";

$sql  = $conn->prepare("insert into autos VALUES (:autokenteken, :automerk, :autotype, :autokmstand, :klant_id)");

$sql->execute([
    "autokenteken" => $kenteken,
    "automerk" => $automerk,
    "autotype" => $autotype,
    "autokmstand" => $kilometerstand,
    "klant_id" => $klant_id

]);



echo "De klant is toegevoegd <br/>";
echo "<a href='garagemenu.html'>Terug naar het menu </a>";
